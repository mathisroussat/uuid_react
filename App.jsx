import React, { useState, useEffect } from 'react';

const BluetoothComponent = () => {
  const [devices, setDevices] = useState([]);
  const [selectedDevice, setSelectedDevice] = useState(null);
  const [characteristicValue, setCharacteristicValue] = useState(null);
  const [connecting, setConnecting] = useState(false);
  
  const discoverDevices = async () => {
    try {
      // Demander à l'utilisateur l'autorisation d'activer le Bluetooth
      const device = await navigator.bluetooth.requestDevice({
        acceptAllDevices: true,
        optionalServices: ["4fafc201-1fb5-459e-8fcc-c5c9c331914b"],
      });

      // Mettre à jour la liste des périphériques découverts
      setDevices((prevDevices) => [...prevDevices, device]);
      setSelectedDevice(device);
    } catch (error) {
      console.error('Erreur lors de la découverte des périphériques:', error);
    }
  };

  const connectToDevice = async () => {
    try {
      // Connecter le périphérique sélectionné
      console.log("awaiting for server")
      const server = await selectedDevice.gatt?.connect();
      console.log("waiting for service");
      // Obtenir le service avec l'UUID spécifié
      const service = await server.getPrimaryService('4fafc201-1fb5-459e-8fcc-c5c9c331914b');
      console.log("service found");
      console.log("waiting for chara");
      // Obtenir les caractéristiques du service
      const characteristics = await service?.getCharacteristics();
      console.log("chara found");
      // Trouver la caractéristique avec l'UUID spécifié
      const targetCharacteristic = characteristics.find(
        (char) => char.uuid === 'beb5483e-36e1-4688-b7f5-ea07361b26a8'
      );

      if (targetCharacteristic) {
        // Lire la valeur de la caractéristique
        const value = await targetCharacteristic.readValue();
        setCharacteristicValue(value);
        console.log(value);
            } else {
        throw new Error('Caractéristique non trouvée');
      }
    } catch (error) {
      console.error('Erreur lors de la connexion au périphérique:', error);
    } finally {
      setConnecting(false);
    }
  };

  return (
    <div>
        <div>
          <p>En attente de la connexion au périphérique Bluetooth...</p>
          <button onClick={() => discoverDevices()} disabled={connecting}>
            Découvrir les périphériques Bluetooth
          </button>
          <button onClick={() => connectToDevice(selectedDevice)}>Connecter</button>
        </div>
    </div>
  );
};

export default BluetoothComponent;